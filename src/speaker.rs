use std::{
    sync::mpsc::{channel, Receiver, Sender},
    time::Duration,
};

use cpal::{
    traits::{DeviceTrait, HostTrait, StreamTrait},
    OutputCallbackInfo, SampleRate, Stream, StreamConfig,
};

use crate::source::Source;

pub struct Speaker {
    stream: Option<Stream>,
    cmd: Option<Sender<Cmd>>,
    rpl: Option<Receiver<Rpl>>,
}

enum Cmd {
    IsPaused,
    SetPause(bool),
    Duration,
    Position,
    SetPosition(Duration),
    Stop,
}
enum Rpl {
    Is(bool),
    Time(Duration),
}
impl Speaker {
    pub fn new() -> Speaker {
        Speaker {
            stream: None,
            cmd: None,
            rpl: None,
        }
    }
    pub fn play(&mut self, mut src: Source, on_end: Option<Box<dyn Fn() + Send>>) {
        let (snd, cmd) = channel();
        let (rpl, rcv2) = channel();
        self.cmd.replace(snd);
        self.rpl.replace(rcv2);
        let mut is_paused = false;
        let mut on_end_done = false;
        let mut stopped = false;
        let stream = cpal::default_host()
            .default_output_device()
            .unwrap()
            .build_output_stream(
                &StreamConfig {
                    channels: src.channels() as u16,
                    sample_rate: SampleRate(src.sample_rate()),
                    buffer_size: cpal::BufferSize::Default,
                },
                move |data: &mut [f32], _: &OutputCallbackInfo| {
                    if let Ok(cmd) = cmd.try_recv() {
                        match cmd {
                            Cmd::IsPaused => rpl.send(Rpl::Is(is_paused)).unwrap(),
                            Cmd::SetPause(pause) => is_paused = pause,
                            Cmd::Duration => rpl.send(Rpl::Time(src.duration())).unwrap(),
                            Cmd::Position => rpl.send(Rpl::Time(src.position())).unwrap(),
                            Cmd::SetPosition(position) => src.set_position(position),
                            Cmd::Stop => stopped = true,
                        }
                    }
                    match src.ended() || stopped {
                        true => {
                            if !on_end_done {
                                if let Some(on_end) = on_end.as_ref() {
                                    on_end()
                                }
                                on_end_done = true;
                            }
                            for s in data {
                                *s = 0.;
                            }
                        }
                        false => {
                            if !is_paused {
                                src.write(data);
                            } else {
                                for s in data {
                                    *s = 0.;
                                }
                            }
                        }
                    };
                },
                |err| panic!("{err}"),
                None,
            )
            .unwrap();
        stream.play().unwrap();
        self.stream.replace(stream);
    }
    pub fn is_empty(&self) -> bool {
        self.stream.is_none()
    }
    pub fn is_paused(&self) -> bool {
        match self.cmd.as_ref() {
            Some(cmd) => {
                cmd.send(Cmd::IsPaused).unwrap();
                match self.rpl.as_ref().unwrap().recv().unwrap() {
                    Rpl::Is(paused) => paused,
                    _ => unreachable!(),
                }
            }
            None => false,
        }
    }
    pub fn set_pause(&self, pause: bool) {
        if let Some(cmd) = self.cmd.as_ref() {
            cmd.send(Cmd::SetPause(pause)).unwrap()
        }
    }
    pub fn duration(&self) -> Duration {
        match self.cmd.as_ref() {
            Some(cmd) => {
                cmd.send(Cmd::Duration).unwrap();
                match self.rpl.as_ref().unwrap().recv().unwrap() {
                    Rpl::Time(duration) => duration,
                    _ => unreachable!(),
                }
            }
            None => Duration::from_secs(0),
        }
    }
    pub fn position(&self) -> Duration {
        match self.cmd.as_ref() {
            Some(cmd) => {
                cmd.send(Cmd::Position).unwrap();
                match self.rpl.as_ref().unwrap().recv().unwrap() {
                    Rpl::Time(position) => position,
                    _ => unreachable!(),
                }
            }
            None => Duration::from_secs(0),
        }
    }
    pub fn set_position(&self, position: Duration) {
        if let Some(cmd) = self.cmd.as_ref() {
            cmd.send(Cmd::SetPosition(position)).unwrap()
        }
    }
    pub fn stop(&self) {
        if let Some(cmd) = self.cmd.as_ref() {
            cmd.send(Cmd::Stop).unwrap()
        }
    }
}
