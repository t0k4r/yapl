use std::{fs::File, time::Duration};

use symphonia::{
    core::{
        audio::SampleBuffer,
        codecs::Decoder,
        errors::Error,
        formats::{FormatReader, Packet, SeekMode, SeekTo},
        io::MediaSourceStream,
        units::Time,
    },
    default::{get_codecs, get_probe},
};

pub struct Source {
    fmt: Box<dyn FormatReader>,
    dec: Box<dyn Decoder>,
    ts: u64,
    buf: Vec<f32>,
    end: bool,
}

impl Source {
    pub fn new(file: File) -> Result<Source, Error> {
        let fmt = get_probe()
            .format(
                &Default::default(),
                MediaSourceStream::new(Box::new(file), Default::default()),
                &Default::default(),
                &Default::default(),
            )?
            .format;
        let dec = get_codecs().make(
            &fmt.default_track().unwrap().codec_params,
            &Default::default(),
        )?;
        Ok(Source {
            fmt,
            dec,
            ts: 0,
            buf: vec![],
            end: false,
        })
    }
    fn packet(&mut self) -> Option<Packet> {
        match self.fmt.next_packet() {
            Ok(packet) => {
                self.ts = packet.ts();
                Some(packet)
            }
            Err(_) => None,
        }
    }
    fn decode(&mut self, packet: Packet) -> Vec<f32> {
        match self.dec.decode(&packet) {
            Ok(decoded) => {
                let mut sample_buf =
                    SampleBuffer::<f32>::new(decoded.capacity() as u64, *decoded.spec());
                sample_buf.copy_interleaved_ref(decoded);
                let samples = sample_buf.samples();
                return samples.to_owned();
            }
            Err(err) => {
                panic!("{}", err);
            }
        }
    }

    fn next(&mut self) {
        if let Some(packet) = self.packet() {
            let mut vec = self.decode(packet);
            self.buf.append(vec.as_mut())
        } else {
            self.end = true
        }
    }
    pub fn ended(&self) -> bool {
        self.end
    }
    pub fn write(&mut self, data: &mut [f32]) {
        for s in data {
            if self.buf.len() == 0 {
                self.next();
            };
            if self.buf.len() != 0 {
                *s = self.buf.remove(0)
            }
        }
    }

    pub fn channels(&self) -> usize {
        self.fmt
            .default_track()
            .unwrap()
            .codec_params
            .channels
            .unwrap()
            .count()
    }
    pub fn sample_rate(&self) -> u32 {
        self.fmt
            .default_track()
            .unwrap()
            .codec_params
            .sample_rate
            .unwrap()
    }
    pub fn duration(&self) -> Duration {
        let time = self
            .fmt
            .default_track()
            .unwrap()
            .codec_params
            .time_base
            .unwrap()
            .calc_time(
                self.fmt
                    .default_track()
                    .unwrap()
                    .codec_params
                    .n_frames
                    .unwrap(),
            );
        Duration::from_secs_f64(time.seconds as f64 + time.frac)
    }
    pub fn position(&self) -> Duration {
        let time = self
            .fmt
            .default_track()
            .unwrap()
            .codec_params
            .time_base
            .unwrap()
            .calc_time(self.ts);
        Duration::from_secs_f64(time.seconds as f64 + time.frac)
    }
    pub fn set_position(&mut self, mut position: Duration) {
        self.buf.clear();
        if position > self.duration() {
            position = self.duration()
        }
        let secs = (position.as_millis() / 1_000) as u64;
        let mut frac = (position.as_millis() % 1_000) as f64;
        while frac > 0. {
            frac /= 10.;
        }
        if secs == 0 && frac < 0.1 {
            frac = 0.1
        }
        self.fmt
            .seek(
                SeekMode::Coarse,
                SeekTo::Time {
                    time: Time {
                        seconds: secs,
                        frac: frac,
                    },
                    track_id: None,
                },
            )
            .unwrap();
    }
}
